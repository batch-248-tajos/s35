//Activity

// Registering a user
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the user already exists in the database, we return an error
    - If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json())
app.use(express.urlencoded({extended: true}))

const port = 4000;

mongoose.set('strictQuery', true)
mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.9omufck.mongodb.net/s35?retryWrites=true&w=majority",
	{
		//Allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

let db = mongoose.connection;

//error handling in connecting
db.on("error",console.error.bind(console,"connection error"));

db.once("open",()=>console.log("We're connected to MongoDB Atlas!"))

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    }
})

const User = mongoose.model("User", userSchema)

app.post('/signup', (req, res) => {
    User.findOne({username: req.body.username}, (err, result) => {
        if (result != null && result.username === req.body.username)
            return res.send('Username already exist!')

        if (req.body.password === '' || req.body.username === '')
            return res.send('Username and password must not be empty!')

        const newUser = new User({
            username: req.body.username,
            password: req.body.password
        })

        newUser.save((err, user) => {
            if (err)
                return console.error(err)

            return res.status(201).send("New user registered.")
        })
    })
})

app.listen(port,()=>console.log(`Server running at port ${port}`));